/*

sensormanager.hpp

The SensorManager class

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "sensor.hpp"
#include "usbmodule.hpp"

class SensorManager {
public:
    SensorManager(Sensor ** sensor_list, uint8_t sensor_list_size);
    void begin();

    uint8_t getSensorNum();
    uint16_t readSensor(uint8_t index);

    /* Protocol functions */
    void parseGetSensorInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet);
    void parseGetSensor(hid_read_data_t * in_packet, hid_write_data_t * out_packet);

private:
    Sensor ** sensor_list;
    uint8_t sensor_list_size;
};